#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string>
#include <fstream>
#include "Gamer.h"
#include "ConsoleGamer.h"
#include "RandomGamer.h"
#include "OptimalGamer.h"
#include "optionparser.h"
#include <ctime>

enum  PlayersIndex {Base, Random, Console, Optimal};

struct Arg : public option::Arg
{
	static void printError(const char* msg1, const option::Option& opt, const char* msg2)
	{
		fprintf(stderr, "%s", msg1);
		fwrite(opt.name, opt.namelen, 1, stderr);
		fprintf(stderr, "%s", msg2);
	}
	static option::ArgStatus Required(const option::Option& option, bool msg)
	{
		if (option.arg != 0)
			return option::ARG_OK;

		if (msg) printError("Option '", option, "' requires an argument\n");
		return option::ARG_ILLEGAL;
	}
};

enum  optionIndex { UNKNOWN, HELP, FIRST, SECOND, COUNT, PAUSE };
const option::Descriptor usage[] =
{
{ UNKNOWN, 0,"" , ""    ,option::Arg::None, "USAGE: seabattle2 [options]\n\n"
	"Options:" },

{ HELP,    0,"h" , "help",option::Arg::None, "  --help, -h  \tPrint usage and exit." },
{ COUNT,   0,"c", "count",Arg::Required, "  --count, -c  \tRound Count, default=1" },
{ FIRST,   0,"f", "first",Arg::Required, "  --first, -f  \tFirst player (rand, cons, optim, basic), default=rand" },
{ SECOND,  0,"s", "second",Arg::Required, "  --second, -c  \tSecond player (rand, cons, optim, basic), default=rand" },
{PAUSE,    0,"p" , "pause",option::Arg::None, "  --pause, -p  \tPause after every turn" },

{ UNKNOWN, 0,"" ,  ""   ,option::Arg::None, "\nExamples:\n"
"  seabattle2 -c 1 \n" },
{ 0,0,0,0,0,0 }
};



int main(int argc, char* argv[])
{
	argc -= (argc>0); argv += (argc>0); // skip program name argv[0] if present
	option::Stats  stats(usage, argc, argv);
	option::Option options[10], buffer[10];
	option::Parser parse(usage, argc, argv, options, buffer);
	
	if (parse.error())
	{
		cout << "Parser Error" << endl;
		return 1;
	}

	if (options[HELP] || argc == 0)   // выводим help если у программы не заданы аргументы
	{
		option::printUsage(std::cout, usage);
		return 0;
	}
		
	int RoundsCount = 1; // считываем число раундов
	if (options[COUNT]) 
		RoundsCount = atoi(options[COUNT].arg);
	
	PlayersIndex firstplayertype = Random;
	if (options[FIRST]) {
		string  s = options[FIRST].arg;
		if (s == "rand")
			firstplayertype = Random;
		if (s == "cons")
			firstplayertype = Console;
		if (s == "basic")
			firstplayertype = Base;
		if (s == "optim")
			firstplayertype = Optimal;
	}
		
	PlayersIndex secondplayertype = Random;
	if (options[SECOND]) {
		string  s = options[SECOND].arg;
		if (s == "rand")
			secondplayertype = Random;
		if (s == "cons")
			secondplayertype = Console;
		if (s == "basic")
			secondplayertype = Base;
		if (s == "optim")
			secondplayertype = Optimal;
	}

	int WinP2 = 0;
	int WinP1 = 0;

	for (int r = 0; r < RoundsCount; r++) // цикл раундов
	{


		Gamer *pGamer1;
		Gamer *pGamer2;

		switch (firstplayertype)
		{
		case Base:
			pGamer1 = new Gamer;
			break;
		case Random:
			pGamer1 = new RandomGamer;
			break;
		case Console:
			pGamer1 = new ConsoleGamer;
			break;
		case Optimal:
			pGamer1 = new OptimalGamer;
			break;
		default:
			pGamer1 = new RandomGamer;
			break;
		}


		switch (secondplayertype)
		{
		case Base:
			pGamer2 = new Gamer;
			break;
		case Random:
			pGamer2 = new RandomGamer;
			break;
		case Console:
			pGamer2 = new ConsoleGamer;
			break;
		case Optimal:
			pGamer2 = new OptimalGamer;
			break;
		default:
			pGamer2 = new RandomGamer;
			break;
		}



		srand(time(0)); // рандомизация (randomize)
		Gamer &P1 = *pGamer1;
		Gamer &P2 = *pGamer2;

		P1.num = 1;
		P2.num = 2;
		if (options[PAUSE])
		{
			P1.printevryturn = true;
			P2.printevryturn = true;
			
		}
		else
		{
			P1.printevryturn = false;
			P2.printevryturn = false;
		}
		
		P1.SetShips();
		P2.SetShips();

		bool NextTurnP1 = true;
		
		while ((P1.hp > 0) && (P2.hp > 0))
		{
			

			if (NextTurnP1)
			{
				//cout << "11111111111111111111111111111111" << endl;
				int pp1 = P1.MakeShot(P2);
				if ((pp1 ==7)||(pp1 ==0))  // мимо
					NextTurnP1 = false;

				//if (options[PAUSE]) пауза после каждого выстрела
				//	cin.get();
			}

			if (!NextTurnP1)
			{
				//cout << "222222222222222222222222222222222" << endl;
				int pp2 = P2.MakeShot(P1);
				if (((pp2 == 7) || (pp2 == 0)))  // мимо
					NextTurnP1 = true;

			/*	if (options[PAUSE])
					cin.get();*/
			}

		}
		if (P1.hp == 0)
		{
			cout << "Player2 pobedil";
			WinP2++;
		}
		
		else
		{
			cout << "Player1 pobedil";
			WinP1++;
		}
		cin.get();
		delete pGamer1;
		delete pGamer2;
	}

	cout << "p1 vs p2 :   "<< WinP1 << ":" << WinP2 << endl;
	cout << "\nPress Enter to continue\n";
	cin.get();

	return 0;
}


