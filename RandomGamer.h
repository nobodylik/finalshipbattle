#pragma once

#include "Gamer.h"

class RandomGamer :
	public Gamer
{
public:
	void SetShips();
	int MakeShot(Gamer &Enemy);

	RandomGamer(string s = "");
	~RandomGamer();
protected:
	void SetShip4();
	int SetShip3();
	int SetShip2();
	int SetShip1();
};
