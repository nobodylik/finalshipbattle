
#include "RandomGamer.h"

void RandomGamer::SetShip4()
{
	int napravl;
	int gorizontal;
	int vertikal;
	napravl = get_rand(1000); // 0- по вертикали, 1 - по горизонлати

	if ((napravl % 2) == 1)
	{
		gorizontal = get_rand(6);
		vertikal = get_rand(9);
		for (int i = gorizontal; i < gorizontal + 4; i++)
		{
			myTable[i][vertikal] = 4;
			mytestTable[i][vertikal] = 9;
		}

	}
	else
	{
		gorizontal = get_rand(9);
		vertikal = get_rand(6);
		for (int i = vertikal; i < vertikal + 4; i++)
		{
			myTable[gorizontal][i] = 4;
			mytestTable[gorizontal][i] = 9;
		}

	}

	if ((napravl % 2) == 1)
	{
		for (int i = gorizontal; i < gorizontal + 4; i++)
			Obhod(i, vertikal, mytestTable);
	}
	else
	{
		for (int i = vertikal; i < vertikal + 4; i++)
			Obhod(gorizontal, i, mytestTable);
	}

}

int RandomGamer::SetShip3()
{
	int napravl;
	int gorizontal;
	int vertikal;
	napravl = get_rand(1000); //0- по вертикали 1 - по горизонлати

	if ((napravl % 2) == 1)
	{
		gorizontal = get_rand(7);
		vertikal = get_rand(9);
		for (int i = gorizontal; i < gorizontal + 3; i++)
		{
			if (mytestTable[i][vertikal] != 9)
			{
				myTable[i][vertikal] = 3;
				mytestTable[i][vertikal] = 9;
			}
			else // стираем то, что уже нарисовали
			{
				for (int j = gorizontal; j < i; j++)
				{
					myTable[j][vertikal] = 0;
					mytestTable[j][vertikal] = 0;
				}
				return -1;
			}
		}

	}
	else
	{
		gorizontal = get_rand(9);
		vertikal = get_rand(7);
		for (int i = vertikal; i < vertikal + 3; i++)
		{
			if (mytestTable[gorizontal][i] != 9) //////////////////
			{
				myTable[gorizontal][i] = 3;
				mytestTable[gorizontal][i] = 9;
			}
			else
			{
				for (int j = vertikal; j < i; j++)
				{
					myTable[gorizontal][j] = 0;
					mytestTable[gorizontal][j] = 0;
				}
				return -1;
			}
		}

	}

	if ((napravl % 2) == 1)
	{
		for (int i = gorizontal; i < gorizontal + 3; i++)
			Obhod(i, vertikal, mytestTable);
	}
	else
	{
		for (int i = vertikal; i < vertikal + 3; i++)
			Obhod(gorizontal, i, mytestTable);
	}
	return 1;
}

int RandomGamer::SetShip2()
{
	int napravl;
	int gorizontal;
	int vertikal;
	napravl = get_rand(1000); //0- по вертикали 1 - по горизонлати

	if ((napravl % 2) == 1)
	{
		gorizontal = get_rand(8);
		vertikal = get_rand(9);
		for (int i = gorizontal; i < gorizontal + 2; i++)
		{
			if (mytestTable[i][vertikal] != 9)
			{
				myTable[i][vertikal] = 2;
				mytestTable[i][vertikal] = 9;
			}
			else // стираем то, что уже нарисовали
			{
				for (int j = gorizontal; j < i; j++)
				{
					myTable[j][vertikal] = 0;
					mytestTable[j][vertikal] = 0;
				}
				return -1;
			}
		}

	}
	else
	{
		gorizontal = get_rand(9);
		vertikal = get_rand(8);
		for (int i = vertikal; i < vertikal + 2; i++)
		{
			if (mytestTable[gorizontal][i] != 9) //////////////////
			{
				myTable[gorizontal][i] = 2;
				mytestTable[gorizontal][i] = 9;
			}
			else
			{
				for (int j = vertikal; j < i; j++)
				{
					myTable[gorizontal][j] = 0;
					mytestTable[gorizontal][j] = 0;
				}
				return -1;
			}
		}

	}

	if ((napravl % 2) == 1)
	{
		for (int i = gorizontal; i < gorizontal + 2; i++)
			Obhod(i, vertikal, mytestTable);
	}
	else
	{
		for (int i = vertikal; i < vertikal + 2; i++)
			Obhod(gorizontal, i, mytestTable);
	}
	return 1;
}

int RandomGamer::SetShip1()
{
	int napravl;
	int gorizontal;
	int vertikal;
	napravl = get_rand(1000); //0- по вертикали 1 - по горизонлати

	if ((napravl % 2) == 1)
	{
		gorizontal = get_rand(9);
		vertikal = get_rand(9);
		for (int i = gorizontal; i < gorizontal + 1; i++)
		{
			if (mytestTable[i][vertikal] != 9)
			{
				myTable[i][vertikal] = 1;
				mytestTable[i][vertikal] = 9;
			}
			else // стираем то, что уже нарисовали
			{
				for (int j = gorizontal; j < i; j++)
				{
					myTable[j][vertikal] = 0;
					mytestTable[j][vertikal] = 0;
				}
				return -1;
			}
		}

	}
	else
	{
		gorizontal = get_rand(9);
		vertikal = get_rand(9);
		for (int i = vertikal; i < vertikal + 1; i++)
		{
			if (mytestTable[gorizontal][i] != 9) //////////////////
			{
				myTable[gorizontal][i] = 1;
				mytestTable[gorizontal][i] = 9;
			}
			else
			{
				for (int j = vertikal; j < i; j++)
				{
					myTable[gorizontal][j] = 0;
					mytestTable[gorizontal][j] = 0;
				}
				return -1;
			}
		}

	}

	if ((napravl % 2) == 1)
	{
		for (int i = gorizontal; i < gorizontal + 1; i++)
			Obhod(i, vertikal, mytestTable);
	}
	else
	{
		for (int i = vertikal; i < vertikal + 1; i++)
			Obhod(gorizontal, i, mytestTable);
	}
	return 1;
}

void RandomGamer::SetShips()
{
	SetShip4();
	for (int i = 1; i < 3; i++)
		while (SetShip3() != 1);
	for (int i = 1; i < 4; i++)
		while (SetShip2() != 1);
	for (int i = 1; i < 5; i++)
		while (SetShip1() != 1);

	findHP();
}

int RandomGamer::MakeShot(Gamer &Enemy)
{
	int x = 0;
	int y = 0;
	x = get_rand(10);
	y = get_rand(10);
	
	while (Enemy.myTable[x][y] == 9 || Enemy.myTable[x][y]<0)
	{
		x = get_rand(10);
		y = get_rand(10);
	}
	int t = Shot(Enemy, x, y);

	switch (t)
	{
	case 0:
	{
		Enemy.PrintEnemy();
		cout << otst() << "randomplayer  ne popal v kletky {" << x << ":" << y << "}" << endl;
		break;
	}
	case 1:
	{
		Enemy.PrintEnemy();
		cout << otst() << "randomplayer popal v kletky {" << x << ":" << y << "}" << endl;
		break;
	}
	case 2:
	{
		Enemy.PrintEnemy();
		cout << otst() << "randomplayer ybil korabl v {" << x << ":" << y << "}" << endl;
		break;
	}
	case 7:
	{
		Enemy.PrintEnemy();
		cout << otst() << "randomplayer povtorno strelaet v {" << x << ":" << y << "}" << endl;
		break;
	}
	default:
		break;
	}

	return t;
}
RandomGamer::RandomGamer(string s) : Gamer(s)
{
}

RandomGamer::~RandomGamer()
{
}