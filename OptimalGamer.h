#pragma once
#include "Gamer.h"
class Tochka
{
public:
	Tochka()
	{
		empty = true;
	}
	int x;
	int y;
	bool empty;
};

class OptimalGamer :
	public Gamer
{
public:
	int MakeShot(Gamer &Enemy);
	OptimalGamer();
	virtual ~OptimalGamer();
protected:
	bool dobivanie;
	Tochka lasthit;
	Tochka gr1;
	Tochka gr2;
};

