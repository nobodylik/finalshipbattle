
#include "ConsoleGamer.h"

void ConsoleGamer::SetShips4()
{
	int x = 0;
	int y = 0;
	char vect;
	cout << otst() << "ship 4 #1 " << endl;

	cout << otst() << "vvedite x ";
	cin >> x;
	cout << otst() << "vvedite y ";
	cin >> y;

	while (x > 10 || x < 1)
	{
		cout << otst() << "x nekorektno, vvedite x povtorno " << endl;
		cout << otst() << "vvedite x ";
		cin >> x;
	}
	while (y > 10 || y < 1)
	{
		cout << otst() << "y nekorektno, vvedite y povtorno " << endl;
		cout << otst() << "vvedite y ";
		cin >> y;
	}
	cout << otst() << "vvedite napravlenie (u d l r) ";
	cin >> vect;


	switch (vect)
	{
	case 'l':
		myTable[x][y] = 4;
		myTable[x - 1][y] = 4;
		myTable[x - 2][y] = 4;
		myTable[x - 3][y] = 4;
		break;
	case 'r':
		myTable[x][y] = 4;
		myTable[x + 1][y] = 4;
		myTable[x + 2][y] = 4;
		myTable[x + 3][y] = 4;
		break;
	case 'u':
		myTable[x][y] = 4;
		myTable[x][y - 1] = 4;
		myTable[x][y - 2] = 4;
		myTable[x][y - 3] = 4;
		break;
	case 'd':
		myTable[x][y] = 4;
		myTable[x][y + 1] = 4;
		myTable[x][y + 2] = 4;
		myTable[x][y + 3] = 4;
		break;
	default:
		break;
	}
	Print();
	cout << endl;
}

void ConsoleGamer::SetShips3()
{
	for (int i = 1; i <= 2; i++)
	{
		int x = 0;
		int y = 0;
		char vect;
		cout << otst() << "ship 3  #" << i << endl;
		cout << otst() << "vvedite x ";
		cin >> x;
		cout << otst() << "vvedite y ";
		cin >> y;
		cout << otst() << "vvedite napravlenie (u d l r) ";
		cin >> vect;

		while (x > 10 || x < 1)
		{
			cout << otst() << "x nekorektno, vvedite x povtorno " << endl;
			cout << otst() << "vvedite x ";
			cin >> x;
		}
		while (y > 10 || y < 1)
		{
			cout << otst() << "y nekorektno, vvedite y povtorno " << endl;
			cout << otst() << "vvedite y ";
			cin >> y;
		}

		switch (vect)
		{
		case 'l':
			myTable[x][y] = 3;
			myTable[x - 1][y] = 3;
			myTable[x - 2][y] = 3;
			break;
		case 'r':
			myTable[x][y] = 3;
			myTable[x + 1][y] = 3;
			myTable[x + 2][y] = 3;
			break;
		case 'u':
			myTable[x][y] = 3;
			myTable[x][y - 1] = 3;
			myTable[x][y - 2] = 3;
			break;
		case 'd':
			myTable[x][y] = 3;
			myTable[x][y + 1] = 3;
			myTable[x][y + 2] = 3;
			break;
		default:
			break;
		}
		Print();
		cout << endl;
	}
}

void ConsoleGamer::SetShips2()
{
	for (int i = 1; i <= 3; i++)
	{
		int x = 0;
		int y = 0;
		char vect;
		cout << otst() << "ship 2  #" << i << endl;
		cout << otst() << "vvedite x ";
		cin >> x;
		cout << otst() << "vvedite y ";
		cin >> y;
		cout << otst() << "vvedite napravlenie (u d l r) ";
		cin >> vect;

		while (x > 10 || x < 1)
		{
			cout << otst() << "x nekorektno, vvedite x povtorno " << endl;
			cout << otst() << "vvedite x ";
			cin >> x;
		}
		while (y > 10 || y < 1)
		{
			cout << otst() << "y nekorektno, vvedite y povtorno " << endl;
			cout << otst() << "vvedite y ";
			cin >> y;
		}

		switch (vect)
		{
		case 'l':
			myTable[x][y] = 2;
			myTable[x - 1][y] = 2;
			break;
		case 'r':
			myTable[x][y] = 2;
			myTable[x + 1][y] = 2;
			break;
		case 'u':
			myTable[x][y] = 2;
			myTable[x][y - 1] = 2;
			break;
		case 'd':
			myTable[x][y] = 2;
			myTable[x][y + 1] = 2;
			break;
		default:
			break;
		}
		Print();
		cout << endl;
	}
}

void ConsoleGamer::SetShips1()
{
	for (int i = 1; i <= 4; i++)
	{
		int x = 0;
		int y = 0;
		cout << otst() << "ship 2  #" << i << endl;
		cout << otst() << "vvedite x ";
		cin >> x;
		cout << otst() << "vvedite y ";
		cin >> y;

		while (x > 10 || x < 1)
		{
			cout << otst() << "x nekorektno, vvedite x povtorno " << endl;
			cout << otst() << "vvedite x ";
			cin >> x;
		}
		while (y > 10 || y < 1)
		{
			cout << otst() << "y nekorektno, vvedite y povtorno " << endl;
			cout << otst() << "vvedite y ";
			cin >> y;
		}

		myTable[x][y] = 1;

		Print();
		cout << endl;
	}
}

void ConsoleGamer::SetShips()
{
	SetShips4();
	SetShips3();
	SetShips2();
	SetShips1();

	findHP();
}

int ConsoleGamer::MakeShot(Gamer &Enemy)
{
	int x = 0;
	int y = 0;
	cout << endl << otst() << "kyda strelyaem? " << endl;
	cout << otst() << "vvedite x ";
	cin >> x;
	cout << otst() << "vvedite y ";
	cin >> y;

	while (x > 10 || x < 1)
	{
		cout << otst() << "x nekorektno, vvedite x povtorno " << endl;
		cout << otst() << "vvedite x ";
		cin >> x;
	}
	while (y > 10 || y < 1)
	{
		cout << otst() << "y nekorektno, vvedite y povtorno " << endl;
		cout << otst() << "vvedite y ";
		cin >> y;
	}

	int t = Shot(Enemy, x, y);

	switch (t)
	{
	case 0:
	{
		Enemy.PrintEnemy();
		cout << otst() << "    Mimo" << endl;
		break;
	}
	case 1:
	{
		Enemy.PrintEnemy();
		cout << otst() << "Popal!!!" << endl;
		break;
	}
	case 2:
	{
		Obhoddead(x, y, Enemy);
		Enemy.PrintEnemy();
		cout << endl;
		cout << otst() << "YBIL!!!!!!! " << endl;
		break;
	}
	case 7:
	{
		Enemy.PrintEnemy();
		cout << endl;
		cout << otst() << "yje strelyali, vvedite novie koord " << endl;
		break;
	}
	default:
		break;
	}
	return t;
}

ConsoleGamer::ConsoleGamer(string s) : Gamer(s)
{
}

ConsoleGamer::~ConsoleGamer()
{
}
