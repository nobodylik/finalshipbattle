#include "OptimalGamer.h"

int mymax(int a, int b)
{
	if (a > b)
		return a;
	else
		return b;
}

int mymin(int a, int b)
{
	if (a > b)
		return b;
	else
		return a;
}



int OptimalGamer::MakeShot(Gamer &Enemy)
{
	int x = 0;
	int y = 0;
	if (dobivanie)
	{
		if (gr1.empty || gr2.empty)
		{
			// только одно попадание в корабль - выбираем клетку рядом с предыдущей случайным образом
			while (!((x < 11) && (x > 0) && (y > 0) && (y < 11))||(Enemy.myTable[x][y]==9))
			{

				x = lasthit.x;
				y = lasthit.y;
				int r = get_rand(4);
				switch (r)
				{
				case 1:
				{
					y--;
					break;
				}
				case 2:
				{
					x++;
					break;
				}
				case 3:
				{
					y++;
					break;
				}
				case 4:
				{
					x--;
					break;
				}
				}
			}
		}
		else 
		{
			bool found = false;

			// минимум два попадания в корабль - выбираем клетку в том же направлении 
			if(gr1.x == gr2.x) // вертикально
			{
				for (int d = 1; d <= 3; d++) // перебираем разные смещения от перевой потопленной клетки
				{
					// увеличиваем координату, идем вниз
					int NewX = gr1.x;
					int NewY = gr1.y + d;
					if ((NewY > 0) && (NewY < 11) && (NewX > 0) && (NewX < 11) && (Enemy.myTable[NewX][NewY] != 9))
					{
						if (Enemy.myTable[NewX][NewY] >= 0)
						{
							// не стрелянная клетка
							x = NewX;
							y = NewY;
							found = true;
							break;
						}
					}
					else
					{
						// уперлись в границу поля либо в промах
						break;
					}
				}

				if (!found)
				{
					for (int d = 1; d <= 3; d++) // перебираем разные смещения от перевой потопленной клетки
					{
						// уменьшаем координату, идем вверх
						int NewX = gr1.x;
						int NewY = gr1.y - d;
						if ((NewY > 0) && (NewY < 11) && (NewX > 0) && (NewX < 11) && (Enemy.myTable[NewX][NewY] != 9))
						{
							if (Enemy.myTable[NewX][NewY] >= 0)
							{
								// не стрелянная клетка
								x = NewX;
								y = NewY;
								found = true;
								break;
							}
						}
						else
						{
							// уперлись в границу поля либо в промах
							break;
						}
					}
				}
			}
			else // горизонтально
			{
				for (int d = 1; d <= 3; d++) // перебираем разные смещения от перевой потопленной клетки
				{
					// увеличиваем координату, идем вправо
					int NewX = gr1.x + d;
					int NewY = gr1.y;
					if ((NewY > 0) && (NewY < 11) && (NewX > 0) && (NewX < 11) && (Enemy.myTable[NewX][NewY] != 9))
					{
						if (Enemy.myTable[NewX][NewY] >= 0)
						{
							// не стрелянная клетка
							x = NewX;
							y = NewY;
							found = true;
							break;
						}
					}
					else
					{
						// уперлись в границу поля либо в промах
						break;
					}
				}

				if (!found)
				{
					for (int d = 1; d <= 3; d++) // перебираем разные смещения от перевой потопленной клетки
					{
						// уменьшаем координату, идем влево
						int NewX = gr1.x - d;
						int NewY = gr1.y ;
						if ((NewY > 0) && (NewY < 11) && (NewX > 0) && (NewX < 11) && (Enemy.myTable[NewX][NewY] != 9))
						{
							if (Enemy.myTable[NewX][NewY] >= 0)
							{
								// не стрелянная клетка
								x = NewX;
								y = NewY;
								found = true;
								break;
							}
						}
						else
						{
							// уперлись в границу поля либо в промах
							break;
						}
					}
				}
			}

		}
	}
	else
	{
		x = get_rand(10);
		y = get_rand(10);
		while(Enemy.myTable[x][y]==9|| Enemy.myTable[x][y]<0)
		{
			x = get_rand(10);
			y = get_rand(10);
		}
	
		//x = 1;
		//y = 1;
	}

	int t = Shot(Enemy, x, y);
	switch (t)
	{
		case 0:
		{
			Enemy.PrintEnemy();
			cout << otst() << "optimalplayer  ne popal v kletky {" << x << ":" << y << "}" << endl;
			break;
		}
		case 1:
		{
			if (dobivanie)
			{
				// добивание уже идет
				gr1.x = lasthit.x;
				gr1.y = lasthit.y;
				gr1.empty = false;
				
				gr2.x = x;
				gr2.y = y;
				gr2.empty = false;
			}
				
			Enemy.PrintEnemy();
			cout << otst() << "optimalplayer popal v kletky {" << x << ":" << y << "}" << endl;

			dobivanie = true; // начали добиване

			lasthit.x = x;
			lasthit.y = y;
	

			break;
		}
		case 2:
		{
			Obhoddead(x, y, Enemy);
			Enemy.PrintEnemy();
			cout << otst() << "optimalplayer ybil korabl v {" << x << ":" << y << "}" << endl;
			
			dobivanie = false; // закончили добивание
			gr1.empty = true;
			gr2.empty = true;

			lasthit.x = x;
			lasthit.y = y;


			break;
		}
		case 7:
		{

			break;
		}
		default:
			break;
	}
	return t;
}



OptimalGamer::OptimalGamer()
{
	dobivanie = false;
}


OptimalGamer::~OptimalGamer()
{
}
