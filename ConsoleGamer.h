#pragma once

#include "Gamer.h"

class ConsoleGamer :
	public Gamer
{
public:

	void SetShips();
	int MakeShot(Gamer &Enemy);
	ConsoleGamer(string s = "");
	~ConsoleGamer();
protected:

	void SetShips4();
	void SetShips3();
	void SetShips2();
	void SetShips1();
};