#include "Gamer.h"


Gamer::Gamer(string s)
{
	name = s;
	printevryturn = true;
	myTable.resize(11);
	mytestTable.resize(11);
	for (int i = 1; i < 11; i++)
	{
		myTable[i].resize(11);
		mytestTable[i].resize(11);
	}
		
	lastX = 1;
	lastY = 0;
}


Gamer::~Gamer()
{
}

const double RAND_MAX_F = RAND_MAX;
int get_rand(int diap) // 
{
	return rand()%diap +1;

	//return rand() / RAND_MAX_F * diap + 1;
}

void Gamer::Print()
{
//	cout << name << " table:" << endl;
	for (int i = 1; i < 11; i++)
	{
		cout << otst();
		for (int j = 1; j < 11; j++)
			switch (myTable[j][i])
			{
			case 0:
				cout << "." << " ";
				break;
			case -4:
			case -3:
			case -2:
			case -1:
				cout << "X" << " ";
				break;
			case 9:
				cout << "*" << " ";
				break;
			default:
				cout << myTable[j][i] << " ";
			}
		cout << endl;
	}

}

int Gamer::findship(int xx, int yy)
{
	int y = yy;
	int x = xx - 1;
	if ((x != 11) && (x != 0) && (y != 0) && (y != 11) && (myTable[x][y] == -myTable[xx][yy]))
		return 1;  // слева

	y = yy;
	x = xx + 1;
	if ((x != 11) && (x != 0) && (y != 0) && (y != 11) && (myTable[x][y] == -myTable[xx][yy]))
		return 1; // справа

	y = yy - 1;
	x = xx;
	if ((x != 11) && (x != 0) && (y != 11) && (y != 0)&&(myTable[x][y] == -myTable[xx][yy]))
		return 1; // сверху

	y = yy + 1;
	x = xx;
	if ((x != 11) && (x != 0) && (y != 11) && (y != 0)&&(myTable[x][y] == -myTable[xx][yy]))
		return 1; // снизу

	y = yy - 1;
	x = xx - 1;
	if ((x != 11) && (x != 0) && (y != 11) && (y != 0)&&(myTable[x][y] == -myTable[xx][yy]))
		return 1; // слева снизу

	y = yy - 1;
	x = xx + 1;
	if ( (x != 11) && (x != 0) && (y != 11) && (y != 0)&&(myTable[x][y] == -myTable[xx][yy]) )
		return 1; // справа сверху

	y = yy + 1;
	x = xx - 1;
	if ( (x != 11) && (x != 0) && (y != 11) && (y != 0)&&(myTable[x][y] == -myTable[xx][yy]) )
		return 1; // слева снизу

	y = yy + 1;
	x = xx + 1;
	if ( (x != 11) && (x != 0) && (y != 11) && (y != 0)&&(myTable[x][y] == -myTable[xx][yy]) )
		return 1; // справа снизу

	return 0;
}

void Gamer::testPrint()
{
	
	//cout << name << " table:" << endl;
	for (int i = 1; i < 11; i++)
	{
		cout << otst();
		for (int j = 1; j < 11; j++)
			switch (mytestTable[j][i])
			{
			case 0:
				cout << "." << " ";
				break;
			case 9:
				cout << "*" << " ";
				break;
			default:
				cout << mytestTable[j][i] << " ";
			}
		cout << endl;
	}

}

void Gamer::PrintEnemy()
{
	if (!printevryturn)
		return;

	for (int i = 1; i < 11; i++)
	{
		cout << otstEnemy();
		for (int j = 1; j < 11; j++)
			switch (myTable[j][i])
			{
			case 0:
				cout << "." << " ";
				break;
			case -4:
			case -3:
			case -2:
			case -1:
				cout << "X" << " ";
				break;
			case 9:
				cout << "*" << " ";
				break;
			default:
				cout << "." << " ";
			}
		cout << endl;
	}
	cout << otstEnemy() << "HP ostalos: " << hp << endl;

}

void Gamer::SetShips()
{
	cout << otst();
	cout << name << ": " << "Set Ships done! " << endl;



	int x = 0;
	int y = 0;




	myTable[3][5] = 1;
	myTable[1][4] = 1;
	myTable[5][2] = 1;
	myTable[7][1] = 1;

	myTable[10][4] = 4;
	myTable[10][5] = 4;
	myTable[10][6] = 4;
	myTable[10][7] = 4;

	myTable[1][10] = 3;
	myTable[1][9] = 3;
	myTable[1][8] = 3;

	myTable[8][5] = 3;
	myTable[7][5] = 3;
	myTable[6][5] = 3;

	myTable[10][10] = 2;
	myTable[9][10] = 2;

	myTable[4][9] = 2;
	myTable[4][10] = 2;

	myTable[6][7] = 2;
	myTable[7][7] = 2;

	findHP();
}

int Gamer::MakeShot(Gamer &Enemy)
{

	int x = lastX;
	int y = lastY + 1;

	if (y == 11)
	{
		x++;
		y = 1;
	}

	lastX = x;
	lastY = y;

	return Shot(Enemy, x, y);
}

int Gamer::Shot(Gamer &Enemy, int x, int y)
{
	if (Enemy.myTable[x][y] == 0)
	{
		Enemy.myTable[x][y] = 9;
		return 0;
	}
	if ((Enemy.myTable[x][y] >= 1) && (Enemy.myTable[x][y] <= 4))
	{
		Enemy.myTable[x][y] = -Enemy.myTable[x][y]; // попадпние, продолжаем стрельбу
		Enemy.hp--;
		
		if (Enemy.IsShipDead(x, y))
			return 2; // убили
		else
			return 1; // ранили
		


	}
	if((Enemy.myTable[x][y] == 9)||(Enemy.myTable[x][y]<0))
		return 7; // нужно выстрелить повторно

	return 0;
}


void Gamer::findHP()
{
	hp = 0;
	for (int i = 1; i < 11; i++)
	{
		for (int j = 1; j < 11; j++)
			if (myTable[i][j])
				hp++;
	}
}



void miniObhod(int xx, int yy, Gamer &Enemy)
{
	for (int i = xx - 1; i <= xx + 1; i++)
	{
		for (int j = yy - 1; j <= yy + 1; j++)
		{
			if (((i < 11) && (i > 0) && (j > 0) && (j < 11)) && (Enemy.myTable[i][j] >= 0))
			Enemy.myTable[i][j] = 9;
		}
	}
}

void Gamer::Obhod(int xx, int yy, vector <vector <int>> &table)
{


	int y = yy;
	int x = xx - 1;
	if ((x != 11) && (x != 0) && (y != 11) && (y != 0) && (table[x][y] == 0))
		table[x][y] = 9; // слева

	y = yy;
	x = xx + 1;
	if ((x != 11) && (x != 0) && (y != 11) && (y != 0) && (table[x][y] == 0))
		table[x][y] = 9; // справа

	y = yy - 1;
	x = xx;
	if ((x != 11) && (x != 0) && (y != 11) && (y != 0) && (table[x][y] == 0))
		table[x][y] = 9; // сверху

	y = yy + 1;
	x = xx;
	if ((x != 11) && (x != 0) && (y != 11) && (y != 0) && (table[x][y] == 0))
		table[x][y] = 9; // снизу

	y = yy - 1;
	x = xx - 1;
	if ((x != 11) && (x != 0) && (y != 11) && (y != 0) && (table[x][y] == 0))
		table[x][y] = 9; // слева снизу

	y = yy - 1;
	x = xx + 1;
	if ((x != 11) && (x != 0) && (y != 11) && (y != 0) && (table[x][y] == 0))
		table[x][y] = 9; // справа сверху

	y = yy + 1;
	x = xx - 1;
	if ((x != 11) && (x != 0) && (y != 11) && (y != 0) && (table[x][y] == 0))
		table[x][y] = 9; // слева снизу

	y = yy + 1;
	x = xx + 1;
	if ((x != 11) && (x != 0) && (y != 11) && (y != 0) && (table[x][y] == 0))
		table[x][y] = 9; // справа снизу
} // закрашивение клеток рядом  с кораблем


int Gamer::Obhoddead(int xx, int yy, Gamer &Enemy)
{
	
	int x = 0; 
	int y = 0;
	int delta = 0;

	// Этап 1,  y++
	for (delta=0; delta<4; delta++)
	{
		x = xx;
		y = yy + delta;

		if (x<1 || x>10 || y<1 || y>10 ) /// уперлись в границу, переходим к следующему этапу
			break;
		
		if (Enemy.myTable[x][y] <= -1 && Enemy.myTable[x][y] >= -4)/// есть убитая клетка, продолжаем этот этап
		{
			miniObhod(x, y, Enemy);
			continue;
		}
			
		
		break; //дошли до клетки любого другого типа, переходим на следующий этап
	}

	// Этап 2,  y--
	for (delta = 0; delta<4; delta++)
	{
		x = xx;
		y = yy - delta;

		if (x<1 || x>10 || y<1 || y>10) /// уперлись в границу, переходим к следующему этапу
			break;

		if (Enemy.myTable[x][y] <= -1 && Enemy.myTable[x][y] >= -4) /// есть убитая клетка, продолжаем этот этап
		{
			miniObhod(x, y, Enemy);
			continue;
		}

		break; //дошли до клетки любого другого типа, переходим на следующий этап
	}

	// Этап 3,  x++
	for (delta = 0; delta<4; delta++)
	{
		x = xx + delta;
		y = yy;

		if (x<1 || x>10 || y<1 || y>10) /// уперлись в границу, переходим к следующему этапу
			break;

		if (Enemy.myTable[x][y] <= -1 && Enemy.myTable[x][y] >= -4) /// есть убитая клетка, продолжаем этот этап
		{
			miniObhod(x, y, Enemy);
			continue;
		}

		break; //дошли до клетки любого другого типа, переходим на следующий этап
	}

	// Этап 4,  x--
	for (delta = 0; delta<4; delta++)
	{
		x = xx - delta;
		y = yy;

		if (x<1 || x>10 || y<1 || y>10) /// уперлись в границу, переходим к следующему этапу
			break;

		if (Enemy.myTable[x][y] <= -1 && Enemy.myTable[x][y] >= -4) /// есть убитая клетка, продолжаем этот этап
		{
			miniObhod(x, y, Enemy);
			continue;
		}

		break; //дошли до клетки любого другого типа, переходим на следующий этап
	}

	return 1;
} // закрашивение клеток рядом  с кораблем



int Gamer::IsShipDead(int xx, int yy)
{
	int x = 0; 
	int y = 0;
	int delta = 0;

	// Этап 1,  y++
	for (delta=0; delta<4; delta++)
	{
		x = xx;
		y = yy + delta;

		if (x<1 || x>10 || y<1 || y>10 ) /// уперлись в границу, переходим к следующему этапу
			break;
		
		if (myTable[x][y] >= 1 && myTable[x][y] <= 4) /// есть живая клетка, корабль жив, выход
			return 0;
		
		if (myTable[x][y] <= -1 && myTable[x][y] >= -4) /// есть убитая клетка, продолжаем этот этап
			continue;
		
		break; //дошли до клетки любого другого типа, переходим на следующий этап
	}

	// Этап 2,  y--
	for (delta = 0; delta<4; delta++)
	{
		x = xx;
		y = yy - delta;

		if (x<1 || x>10 || y<1 || y>10) /// уперлись в границу, переходим к следующему этапу
			break;

		if (myTable[x][y] >= 1 && myTable[x][y] <= 4) /// есть живая клетка, корабль жив, выход
			return 0;

		if (myTable[x][y] <= -1 && myTable[x][y] >= -4) /// есть убитая клетка, продолжаем этот этап
			continue;

		break; //дошли до клетки любого другого типа, переходим на следующий этап
	}

	// Этап 3,  x++
	for (delta = 0; delta<4; delta++)
	{
		x = xx + delta;
		y = yy;

		if (x<1 || x>10 || y<1 || y>10) /// уперлись в границу, переходим к следующему этапу
			break;

		if (myTable[x][y] >= 1 && myTable[x][y] <= 4) /// есть живая клетка, корабль жив, выход
			return 0;

		if (myTable[x][y] <= -1 && myTable[x][y] >= -4) /// есть убитая клетка, продолжаем этот этап
			continue;

		break; //дошли до клетки любого другого типа, переходим на следующий этап
	}

	// Этап 4,  x--
	for (delta = 0; delta<4; delta++)
	{
		x = xx - delta;
		y = yy;

		if (x<1 || x>10 || y<1 || y>10) /// уперлись в границу, переходим к следующему этапу
			break;

		if (myTable[x][y] >= 1 && myTable[x][y] <= 4) /// есть живая клетка, корабль жив, выход
			return 0;

		if (myTable[x][y] <= -1 && myTable[x][y] >= -4) /// есть убитая клетка, продолжаем этот этап
			continue;

		break; //дошли до клетки любого другого типа, переходим на следующий этап
	}

	return 1;
}

string Gamer::otst()
{
	if (num == 1)
		return "";
	else
		return "\t\t\t\t\t";
}

string Gamer::otstEnemy()
{
	if (num != 1)
		return "";
	else
		return "\t\t\t\t\t";
}

