#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <ctime>
using namespace std;
	int get_rand(int diap); 
class Gamer
{
public:
	bool printevryturn;
	int lastX;
	int lastY;
	int hp;
	int num; // номер игрока
	vector <vector <int>> mytestTable; // 
	string name;
	vector <vector <int>> myTable;

	string otst();
	string otstEnemy();
	virtual	void SetShips();
	int IsShipDead(int xx, int yy);
	virtual int MakeShot(Gamer &Enemy);
	void Print();
	void testPrint();
	int findship(int x, int y); // проверяет, есть ли рядом с клеткой клетка, принадлежащая кораблю, чтобы сказать что корабль уничтожен/нет
	void PrintEnemy();
	int Obhoddead(int x, int y, Gamer &Enemy); // обходит мертвый корабль и закрашивает по периметри
	void Obhod(int x, int y, vector <vector <int>> &table); // обходит корабль и закрашивает по периметри
	Gamer(string nm = "");
	virtual ~Gamer();
protected:

	int Shot(Gamer &Enemy, int x, int y);
	void findHP();
};




